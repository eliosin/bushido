![](https://elioway.gitlab.io/eliosin/bushido/rei/elio-bushido-rei-logo.png)

# 礼

> true warriors have no reason to be cruel. they do not need to prove their strength. warriors are not only respected for their strength in battle, but also by their dealings with others. the true strength of a warrior becomes apparent during difficult times. **Wikipedia**

**rei** means respect. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

This theme is used for **elioWay** module home pages.

![](https://elioway.gitlab.io/eliosin/bushido/rei/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
