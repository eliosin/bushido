const { parallel } = require("gulp")
var exec = require("child_process").exec

function rei(done) {
  exec(
    "gulp --gulpfile ./rei/gulpfile.js build",
    function (err, stdout, stderr) {
      // console.log(stdout);
      // console.log(stderr);
      done(err)
    },
  )
}

const bushido = parallel(rei)

exports.build = bushido
exports.default = bushido
