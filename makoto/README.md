![](https://elioway.gitlab.io/eliosin/bushido/makoto/elio-bushido-makoto-logo.png)

# 誠

> when warriors say that they will perform an action, it is as good as done. nothing will stop them from completing what they say they will do. they do not have to 'give their word'. they do not have to 'promise'. speaking and doing are the same action. **Wikipedia**

**makoto** means honesty. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

![](https://elioway.gitlab.io/eliosin/bushido/makoto/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
