![](https://elioway.gitlab.io/eliosin/bushido/elio-bushido-logo.png)

> Engaged in sin, **the elioWay**

# bushido ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

1. [bushido Documentation](https://elioway.gitlab.io/eliosin/bushido)

## Way of Warriors Index

- [gi](https://gitlab.com/eliosin/bushido/blob/master/chugi/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [yū](https://gitlab.com/eliosin/bushido/blob/master/yu/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [jin](https://gitlab.com/eliosin/bushido/blob/master/jin/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [rei](https://gitlab.com/eliosin/bushido/blob/master/rei/README.html) ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental") Used for elioWay product pages

- [makoto](https://gitlab.com/eliosin/bushido/blob/master/makoto/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [meiyo](https://gitlab.com/eliosin/bushido/blob/master/meiyo/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [chūgi](https://gitlab.com/eliosin/bushido/blob/master/chugi/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

- [jisei](https://gitlab.com/eliosin/bushido/blob/master/jisei/README.html) ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

## Installing

```shell
npm install @elioway/bushido --save
yarn add  @elioway/bushido --dev
```

- [Installing bushido](https://elioway.gitlab.io/eliosin/bushido/installing.html)

## Nutshell

### `gulp`

### `npm run test`

### `npm run prettier`

- [bushido Credits](https://elioway.gitlab.io/eliosin/bushido/credits.html)

![](https://elioway.gitlab.io/eliosin/bushido/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
