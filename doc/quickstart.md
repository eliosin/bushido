# Quickstart bushido

- [bushido Prerequisites](/eliosin/bushido/prerequisites.html)
- [Installing bushido](/eliosin/bushido/installing.html)

## Usage

Once you have installed the bushido theme, you can simply reference the css file in your html files:

```
<head>
  <link rel="stylesheet" href="path/to/rei/rei.min.css" />
</head>
```
