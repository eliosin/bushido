# Contributing

GIT is also a fine way to install **bushido**.

```
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/eliosin.git
cd eliosin
git clone https://gitlab.com/eliosin/bushido.git
cd bushido/<bushido_theme>

set L chugi gi jin jisei makato meiyo rei yu
for R in $L
  ncu -u
  npm i|yarn
  npm link @elioway/innocent
end
```

## TODOS

1. All the themes!
