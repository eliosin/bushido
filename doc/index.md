<aside>
  <dl>
  <dt>武士道</dt>
  <dd>the Way of Warriors</dd>
  <dt>Bushidō</dt>
  <dd>is a Japanese collective term for the many codes of honour and ideals that dictated the samurai way of life, loosely analogous to the indigenous European concept of chivalry. <strong>Wikipedia</strong></dd>
</dl>
</aside>

**bushido** is a collection of elegant, usable eliosin themes named after (and in an act which is the antithesis of) the code of Bushidō. Ideal for news, blogs, shops, etc, targeted to a larger screen.

# Theme Paks

<article>
  <a href="/eliosin/bushido/gi/star.png" target="_splash">
  <img src="/eliosin/bushido/gi/favicoff.png"/>
  <div>
  <h4>gi</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/yu/star.png" target="_splash">
  <img src="/eliosin/bushido/yu/favicoff.png"/>
  <div>
  <h4>yū</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/jin/star.png" target="_splash">
  <img src="/eliosin/bushido/jin/favicoff.png"/>
  <div>
  <h4>jin</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/rei/star.png" target="_splash">
  <img src="/eliosin/bushido/rei/favicoff.png"/>
  <div>
  <h4>rei</h4>
  <p>This page is uses the <strong>rei</strong> theme.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/makoto/star.png" target="_splash">
  <img src="/eliosin/bushido/makoto/favicoff.png"/>
  <div>
  <h4>makoto</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/meiyo/star.png" target="_splash">
  <img src="/eliosin/bushido/meiyo/favicoff.png"/>
  <div>
  <h4>meiyo</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/chugi/star.png" target="_splash">
  <img src="/eliosin/bushido/chugi/favicoff.png"/>
  <div>
  <h4>chūgi</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

<article>
  <a href="/eliosin/bushido/jisei/star.png" target="_splash">
  <img src="/eliosin/bushido/jisei/favicoff.png"/>
  <div>
  <h4>jisei</h4>
  <p>Contributor wanted.</p>
</div>
</a>
</article>

# Contributors wanted

Start contributing to eliosin and work **the elioWay** by adopting on a sinful theme.

# See also

- [eliosin/innocent](/eliosin/innocent)
- [eliosin/sins](/eliosin/sins)
