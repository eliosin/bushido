![](https://elioway.gitlab.io/eliosin/bushido/jisei/elio-bushido-jisei-logo.png)

# 自制

> restraint. **Wikipedia**

**jisei** is restraint. A [bushido](https://elioway.gitlab.io/eliosin/bushido) theme.

![](https://elioway.gitlab.io/eliosin/bushido/jisei/apple-touch-icon.png)

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)
